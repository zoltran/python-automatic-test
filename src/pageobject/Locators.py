class Locators(object):
    #text
    news_text = "NOWOŚCI"
    clothes_text = "Odzież"
    blousesAndShirts_text = "Bluzki i koszule"
    assert_size_clothes_text = "Górne części garderoby 38"
    unpaid_order_text = "Twoje zamówienie jest nieopłacone"
    woman_text = "KOBIETY"
    repay_button_text = "Dokończ płatność"
    size_text = "Rozmiar: 38"
    title_text = "Tommy Hilfiger Koszula Oxford WW0WW32907 Biały Regular Fit"
    web_page_title = "Moda damska, męska i dziecięca • Sklep internetowy MODIVO.PL"
    error_auth = "Błąd autoryzacji"

    #xpath
    xpath_first_product = "//*[@data-product-card=0]"
    xpath_size = "//*[contains(text(),'Rozmiar')]"
    xpath_size_value = "//*[@data-test-value=38]"

    #selector
    accept_button_selector = "[data-test-id=filters-apply-button]"
    category_woman_selector = "[data-test-id=filters-size-category-damskie]"
    jacket_selector = "[data-test-id=filters-size-category-item-damskie_gorne_czesci_garderoby]"
    size_jacket_selector = "label[for=checkbox-38]"
    tag_selector = "div.base-tag"

    choose_product_selector = "[data-test-id=choose-product-size]>.content"
    add_to_cart_button_selector = "[data-test-id=add-to-cart-button]"
    add_product_go_to_summary_selector = "[data-test-id=added-to-cart-go-to-cart-button]"
    card_selector = "input[id=card-number]"
    cardDate_selector = "input[id=card-date]"
    CVV_selector = "input[id=card-cvv]"
    submit_selector = "input[name=submit]"

    check_pay_cart_selector = "a[title=Płatność kartą]"
    form_paymant_selector = "a[title=Podaj dane karty]"

    value_summary_product_selector = "li.value-item"
    proceed_to_checkout_selector = "[data-test-id=cart-proceed-to-checkout]"
    continue_as_guest_selector = "[data-test-id=continue-as-guest]"

    dhl_selector = "img[alt=dhl]"
    payu_selector = "label[for=payu_gateway]>div>div.method-data"
    terms_selector = "label[for=terms]"
    button_agreements_selector = "[data-test-id=agreements-button]"

    #focus
    unpaid_order = "strong.unpaid-order-heading"
    repay_button = "button.repay-btn"
    city_selector = "input[data-test-id=billing__city]"
    email_selector = "input[data-test-id=billing__email]"
    name_selector = "input[data-test-id=billing__firstname]"
    number_house_selector = "input[data-test-id=billing__street-1]"
    phone_selector = "input[data-test-id=billing__telephone]"
    postcode_selector = "input[data-test-id=billing__postcode]"
    street_selector = "input[data-test-id=billing__street-0]"
    surname_selector = "input[data-test-id=billing__lastname]"

    #type
    type_city = "Alwernia"
    type_email = "marek@siewiorek.com.pl"
    type_name = "Testowe"
    type_number_house = "1"
    type_phone = "+48 12 345 67 89"
    type_postcode = "32-566"
    type_street = "Garncarska"
    type_surname = "Zamówienie"
    type_card = "4111111111111111"
    type_cardDate = "11/23"
    type_CVV = "111"

    #other
    selectorNameProduct = "a.text-link.name[title='Tommy Hilfiger Koszula Oxford WW0WW32907 Biały Regular Fit']"

    assert_error = "article.error"

    #start
    button_back = "a.buttonlike"
    url = "https://www.modivo.pl/"
    cookies_selector = "footer>button.secondary"