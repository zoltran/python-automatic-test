from selenium.webdriver.common.by import By


def accept_cookies(self, driver):
    cookies_selector = "footer>button.secondary"

    cookie_notification = driver.find_element(By.CSS_SELECTOR, cookies_selector)
    if cookie_notification:
        cookie_notification.click()
