from src.pageobject.components import CookieNotification
from src.pageobject.pages.CheckoutAccountPage import CheckoutAccountPage
from src.pageobject.pages.CheckoutCartPage import CheckoutCartPage
from src.pageobject.pages.CheckoutPage import CheckoutPage
from src.pageobject.pages.CheckoutSuccessPage import CheckoutSuccessPage
from src.pageobject.pages.HomePage import HomePage
from src.pageobject.pages.SecurityPayuPage import SecurityPayuPage
from src.pageobject.pages.ShowWomanProductPage import ShowWomanProductPage
from src.pageobject.pages.WomanClothesBlousesAndShirtsOffersPage import WomanClothesBlousesAndShirtsOffersPage
from src.pageobject.pages.WomanClothesOffersPage import WomanClothesOffersPage
from src.pageobject.pages.WomanOffersPage import WomanOffersPage
from src.pageobject.pages.WomanPage import WomanPage


class ModivoContext(object):
    def start_page(self, driver):
        HomePage.open(self, driver)
        HomePage.is_loaded(self, driver)
        CookieNotification.accept_cookies(self, driver)

    def navigete_to_blouses_and_shirts_page(self, driver):
        HomePage.click_link_woman(self, driver)
        WomanPage.click_link_news(self, driver)
        WomanOffersPage.click_link_clothes(self, driver)
        WomanClothesOffersPage.click_link_blouses_and_shirts(self, driver)

    def filter_check_size(self, driver):
        WomanClothesBlousesAndShirtsOffersPage.configure_filter(self, driver)
        WomanClothesBlousesAndShirtsOffersPage.click_first_product(self, driver)

    def order_product(self, driver):
        ShowWomanProductPage.set_size(self, driver)
        ShowWomanProductPage.add_product(self, driver)
        CheckoutCartPage.assertSize(self, driver)
        CheckoutCartPage(driver)

    def completing_order(self, driver):
        CheckoutAccountPage.click_continue_as_guest(self, driver)
        CheckoutPage.form_identify(self, driver)
        CheckoutPage.check_order(self, driver)
        CheckoutPage.click_finish_order(self, driver)

    def security_pay_order(self, driver):
        SecurityPayuPage.form_payu_cart(self, driver)
        SecurityPayuPage.assert_payu(self, driver)

    def assertOrderStatus(self, driver):
        CheckoutSuccessPage.assert_summary_payment(self, driver)
