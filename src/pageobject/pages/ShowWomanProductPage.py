from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class ShowWomanProductPage(object):
    def set_size(self, driver):
        driver.find_element(By.CSS_SELECTOR, Locators.choose_product_selector).click()
        driver.find_element(By.XPATH, Locators.xpath_size_value).click()
        driver.find_element(By.CSS_SELECTOR, Locators.add_to_cart_button_selector).click()

    def add_product(self, driver):
        driver.find_element(By.CSS_SELECTOR, Locators.add_product_go_to_summary_selector).click()
