from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class CheckoutSuccessPage(object):
    def assert_summary_payment(self, driver):
        self.assertEqual(driver.find_element(
            By.CSS_SELECTOR, Locators.unpaid_order).text, Locators.unpaid_order_text)
        self.assertEqual(driver.find_element(
            By.CSS_SELECTOR, Locators.repay_button).text, Locators.repay_button_text)
