import time

from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class SecurityPayuPage(object):

    def form_payu_cart(self, driver):
        driver.find_element(By.CSS_SELECTOR, Locators.check_pay_cart_selector).click()
        driver.find_element(By.CSS_SELECTOR, Locators.form_paymant_selector).click()

        card_number = driver.find_element(By.CSS_SELECTOR, Locators.card_selector)
        card_number.send_keys(Locators.type_card)

        cardDate_number = driver.find_element(By.CSS_SELECTOR, Locators.cardDate_selector)
        cardDate_number.send_keys(Locators.type_cardDate)

        CVV_number = driver.find_element(By.CSS_SELECTOR, Locators.CVV_selector)
        CVV_number.send_keys(Locators.type_CVV)

        driver.find_element(By.CSS_SELECTOR, Locators.submit_selector).click()

        time.sleep(3)  # TODO need to be changed

    def assert_payu(self, driver):
        self.assertEqual(driver.find_element(By.CSS_SELECTOR, Locators.assert_error).text, Locators.error_auth)
        driver.find_element(By.CSS_SELECTOR, Locators.button_back).click()
        time.sleep(3)  # TODO need to be changed
