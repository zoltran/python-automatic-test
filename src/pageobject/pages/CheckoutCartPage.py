from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class CheckoutCartPage(object):
    def __init__(self, driver):
        driver.find_element(By.CSS_SELECTOR, Locators.proceed_to_checkout_selector).click()

    def assertSize(self, driver):
        self.assertEqual(driver.find_element(
            By.CSS_SELECTOR, Locators.value_summary_product_selector).text, Locators.size_text)
        self.assertEqual(driver.find_element(
            By.CSS_SELECTOR, Locators.selectorNameProduct).text, Locators.title_text)
