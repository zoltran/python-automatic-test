from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class WomanClothesOffersPage(object):
    def click_link_blouses_and_shirts(self, driver):
        driver.find_element(By.LINK_TEXT, Locators.blousesAndShirts_text).click()
