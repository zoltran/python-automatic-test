import time

from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class CheckoutPage(object):
    def form_identify(self, driver):
        email_element = driver.find_element(By.CSS_SELECTOR, Locators.email_selector)
        email_element.send_keys(Locators.type_email)
        phone_element = driver.find_element(By.CSS_SELECTOR, Locators.phone_selector)
        phone_element.send_keys(Locators.type_phone)
        name_element = driver.find_element(By.CSS_SELECTOR, Locators.name_selector)
        name_element.send_keys(Locators.type_name)
        surname_element = driver.find_element(By.CSS_SELECTOR, Locators.surname_selector)
        surname_element.send_keys(Locators.type_surname)
        street_element = driver.find_element(By.CSS_SELECTOR, Locators.street_selector)
        street_element.send_keys(Locators.type_street)
        numberHouse_element = driver.find_element(By.CSS_SELECTOR, Locators.number_house_selector)
        numberHouse_element.send_keys(Locators.type_number_house)
        postcode_element = driver.find_element(By.CSS_SELECTOR, Locators.postcode_selector)
        postcode_element.send_keys(Locators.type_postcode)
        city_element = driver.find_element(By.CSS_SELECTOR, Locators.city_selector)
        city_element.send_keys(Locators.type_city)

    def check_order(self, driver):
        time.sleep(2) #TODO need to be changed
        driver.find_element(By.CSS_SELECTOR, Locators.dhl_selector).click()
        time.sleep(2) #TODO need to be changed
        driver.find_element(By.CSS_SELECTOR, Locators.payu_selector).click()
        time.sleep(2) #TODO need to be changed
        driver.find_element(By.CSS_SELECTOR, Locators.terms_selector).click()

    def click_finish_order(self, driver):
        driver.find_element(By.CSS_SELECTOR, Locators.button_agreements_selector).click()
        time.sleep(10)  # TODO need to be changed
