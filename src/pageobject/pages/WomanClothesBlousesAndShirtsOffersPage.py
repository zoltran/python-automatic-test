import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from src.pageobject.Locators import Locators


class WomanClothesBlousesAndShirtsOffersPage(object):
    def configure_filter(self, driver):
        driver.find_element(By.XPATH, Locators.xpath_size).click()
        driver.find_element(By.CSS_SELECTOR, Locators.category_woman_selector).click()
        driver.find_element(
            By.CSS_SELECTOR, Locators.jacket_selector).click()
        driver.find_element(By.CSS_SELECTOR, Locators.size_jacket_selector).click()
        driver.find_element(By.CSS_SELECTOR, Locators.accept_button_selector).click()

        wait = WebDriverWait(driver, 15)
        wait.until(isLoadingFilters(
            (By.CSS_SELECTOR, Locators.tag_selector),
            Locators.assert_size_clothes_text))

    def click_first_product(self, driver):
        time.sleep(2)
        driver.find_element(By.XPATH, Locators.xpath_first_product).click()


class isLoadingFilters(object):
    def __init__(self, locator, find_text):
        self.locator = locator
        self.find_text = find_text

    def __call__(self, driver):
        countProducts = driver.find_element(*self.locator).text
        if self.find_text in countProducts:
            return countProducts
        else:
            return False
