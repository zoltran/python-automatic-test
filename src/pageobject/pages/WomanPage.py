from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class WomanPage(object):
    def click_link_news(self, driver):
        driver.find_element(By.LINK_TEXT, Locators.news_text).click()
