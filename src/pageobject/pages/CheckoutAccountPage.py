from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class CheckoutAccountPage(object):
    def click_continue_as_guest(self, driver):
        driver.find_element(By.CSS_SELECTOR, Locators.continue_as_guest_selector).click()
