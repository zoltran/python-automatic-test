from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class HomePage(object):
    def open(self, driver):
        self.driver = driver
        self.driver.get(Locators.url)
        self.driver.set_page_load_timeout(30)

    def is_loaded(self, driver):
        self.assertEqual(driver.title, Locators.web_page_title)

    def click_link_woman(self, driver):
        driver.find_element(By.LINK_TEXT, Locators.woman_text).click()
