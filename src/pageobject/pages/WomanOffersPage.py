from selenium.webdriver.common.by import By

from src.pageobject.Locators import Locators


class WomanOffersPage(object):
    def click_link_clothes(self, driver):
        driver.find_element(By.LINK_TEXT, Locators.clothes_text).click()
