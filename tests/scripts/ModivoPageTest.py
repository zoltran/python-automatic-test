
import sys

from src.pageobject.context.ModivoContext import ModivoContext
from src.pageobject.pages.CheckoutSuccessPage import CheckoutSuccessPage
from src.testbase.WebDriverSetup import WebDriverSetup

sys.path.append(sys.path[0] + "/...")


class ModivoPageTest(WebDriverSetup):

    def test_modivo_page(self):
        # given
        driver = self.driver
        ModivoContext.start_page(self, driver)

        # when
        ModivoContext.navigete_to_blouses_and_shirts_page(self, driver)
        ModivoContext.filter_check_size(self, driver)
        ModivoContext.order_product(self, driver)
        ModivoContext.completing_order(self, driver)
        ModivoContext.security_pay_order(self, driver)

        # then
        ModivoContext.assertOrderStatus(self, driver)